export function createMap27Coder(options?: Options): {
    decode: (message: Uint8Array, length?: number) => {
        decodedMessage: {};
        nextByte: number;
    };
};
export function createTaitCoder(options?: Options): {
    decode: (message: Uint8Array, length?: number) => {
        decodedMessage: {};
        nextByte: number;
    };
};
export function createTaitAsciiCoder(options?: Options): {
    decode: (message: Uint8Array) => {
        decodedMessage: {};
        nextByte: number;
    };
};
export type Options = {
    /**
     * Logger object to pass
     * to the coder
     */
    logger: import('binary-decoder').Logger;
};
