import { sstMessageStructure } from './standard.js';
import {
	addressedMessageStructure,
	addressStructure,
	extendStandard,
	standardMessages
} from './standard.js';

/* @type {import('binary-decoder').MessageStructure} */
const dmrAddressedMessageStructure = [
	{
		type: 'bytes',
		bytes: 3,
		id: 'gatewayDmrId',
		partial: true
	},
	{
		type: 'bitsInt',
		id: 'dmrGatewayIdDetect',
		bits: [0, 1, 7, 3],
		partial: true
	},
	{
		type: 'bitsInt',
		bits: [1, 7, 3, 5, 0, 8],
		id: 'dmrId',
		partial: true
	}
];

/* @type {{[type: string]: import('binary-decoder').MessageStructure}} */
const taitMessages = {
	...standardMessages,
	0x0b: [
		...dmrAddressedMessageStructure,
		...addressedMessageStructure,
		{
			type: 'bits',
			parts: [
				{
					bits: 15
				},
				{
					bits: 1,
					id: 'transmitting'
				}
			]
		}
	],
	0x81: [
		...addressedMessageStructure,
		...addressStructure,
		{
			...sstMessageStructure[0],
			parts: [
				{
					...sstMessageStructure[0].parts[0],
					enums: [
						...sstMessageStructure[0].parts[0].enums,
						{
							value: 7,
							label: 'Tait Proprietary'
						}
					]
				},
				sstMessageStructure[0].parts[1]
			]
		},
		{
			...sstMessageStructure[1],
			structures: {
				...sstMessageStructure[1].structures,
				7: [
					{
						type: 'byte',
						id: 'TaitDataType',
						enums: [
							{
								value: 1,
								label: 'Tait Data Format 1'
							},
							{
								value: 4,
								label: 'NMEA Position'
							},
							{
								value: 5,
								label: 'NMEA Course'
							},
							{
								value: 6,
								label: 'Tait Data Format 1 Position'
							},
							{
								value: 6,
								label: 'Tait Data Format 1 Course'
							}
						]
					}
				]
			}
		}
	],
	0x84: [
		...dmrAddressedMessageStructure,
		...addressedMessageStructure,
		...addressStructure,
		{
			type: 'byte',
			id: 'pollFormat',
			enums: [
				{
					value: 0x00,
					label: 'Binary'
				},
				{
					value: 0x01,
					label: 'MS Address'
				},
				{
					value: 0x02,
					label: '4-bit BCD'
				},
				{
					value: 0x03,
					label: '7-bit ASCII'
				},
				{
					value: 0x04,
					label: '8-bit ASCII'
				},
				{
					value: 0x05,
					label: 'NMEA'
				},
				{
					value: 0x06,
					label: 'IP Address'
				},
				{
					value: 0x07,
					label: '16-bit unicode'
				}
			]
		}
	]
};

// Add dmrID structure to start of message
[0x81, 0x84, 0x86, 0xa4, 0xa6, 0xb0, 0xc4, 0xc5, 0xe0].forEach((messageId) => {
	taitMessages[messageId] = [...dmrAddressedMessageStructure, ...taitMessages[messageId]];
});

export default extendStandard(
	[
		{
			value: 0x84,
			label: 'DMR Poll',
			direction: 'BOTH'
		},
		{
			value: 0x0b,
			label: 'DMR Call Status',
			direction: 'R2D'
		}
	],
	taitMessages
);
