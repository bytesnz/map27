/* @type {import('binary-decoder').MessageStructure} */
export const nmeaStructure = [
	{
		type: 'bits',
		parts: [
			{
				bits: 6
			},
			{
				bits: 2,
				id: 'encrypted',
				enums: [
					{
						value: 0,
						label: 'Not encrypted'
					},
					{
						value: 1,
						label: 'Encrypted'
					}
				]
			}
		]
	},
	{
		type: 'bits',
		parts: [
			{
				bits: 6
			},
			{
				bits: 2,
				id: 'northSouth',
				enums: [
					{
						value: 0,
						label: 'South'
					},
					{
						value: 1,
						label: 'North'
					}
				]
			}
		]
	},
	{
		type: 'bits',
		parts: [
			{
				bits: 6
			},
			{
				bits: 2,
				id: 'EastWest',
				enums: [
					{
						value: 0,
						label: 'West'
					},
					{
						value: 1,
						label: 'East'
					}
				]
			}
		]
	},
	{
		type: 'bits',
		parts: [
			{
				bits: 6
			},
			{
				bits: 2,
				id: 'Quality',
				enums: [
					{
						value: 0,
						label: 'No fix'
					},
					{
						value: 1,
						label: 'Valid fix'
					}
				]
			}
		]
	},
	{
		type: 'byte',
		id: 'speed'
	},
	{
		type: 'uint16',
		id: 'heading'
	},
	{
		type: 'byte',
		id: 'latDegrees'
	},
	{
		type: 'byte',
		id: 'latMinutes'
	},
	{
		type: 'uint16',
		id: 'latFraction'
	},
	{
		type: 'byte',
		id: 'longDegrees'
	},
	{
		type: 'byte',
		id: 'longMinutes'
	},
	{
		type: 'uint16',
		id: 'longFraction'
	},
	{
		type: 'byte',
		id: 'utcHours'
	},
	{
		type: 'byte',
		id: 'utcMinutes'
	},
	{
		type: 'byte',
		id: 'utcSeconds'
	}
];

/* @type {import('binary-decoder').ValueEnum} */
export const standardMessageTypes = [
	{
		value: 0x80,
		label: 'Send/Receive status',
		direction: 'BOTH'
	},
	{
		value: 0x81,
		label: 'Send/Receive short data (SST) message',
		direction: 'BOTH'
	},
	{
		value: 0x82,
		label: 'Send/Receive MST',
		direction: 'BOTH'
	},
	{
		value: 0xa4,
		label: 'Setup/Incoming voice/modem',
		direction: 'BOTH'
	},
	{
		value: 0xa5,
		label: 'Setup/Incoming emergency voice/modem',
		direction: 'BOTH'
	},
	{
		value: 0xc4,
		label: 'Positive Setup Progress',
		direction: 'R2D'
	},
	{
		value: 0xd4,
		label: 'Queuing Setup Progress',
		direction: 'R2D'
	},
	{
		value: 0xe4,
		label: 'Negative Setup Progress',
		direction: 'R2D'
	},
	{
		value: 0xc5,
		label: 'Positive Receive Progress',
		direction: 'R2D'
	},
	{
		value: 0xd5,
		label: 'Warning Receive Progress',
		direction: 'R2D'
	},
	{
		value: 0xe5,
		label: 'Call Not Connected Receive Progress',
		direction: 'R2D'
	},
	{
		value: 0xa3,
		label: 'Send/Receive Modem Data',
		direction: 'BOTH'
	},
	{
		value: 0x86,
		label: 'Normal Disconnect/Clear',
		direction: 'BOTH'
	},
	{
		value: 0xa6,
		label: 'Cancelled Disconnect/Clear',
		direction: 'BOTH'
	},
	{
		value: 0x87,
		label: 'Diversion Request',
		direction: 'D2R'
	},
	{
		value: 0xa7,
		label: 'Diversion Cancelled',
		direction: 'D2R'
	},
	{
		value: 0xc7,
		label: 'Positive Diversion Acknowledgement',
		direction: 'R2D'
	},
	{
		value: 0xe7,
		label: 'Negative Diversion Acknowledgement',
		direction: 'R2D'
	},
	{
		value: 0xb0,
		label: 'Radio Interrogation/Personality',
		direction: 'BOTH'
	},
	{
		value: 0xb1,
		label: 'Numbering Information',
		direction: 'R2D'
	},
	{
		value: 0xb2,
		label: 'Radio/Operating Control',
		direction: 'BOTH'
	},
	{
		value: 0xb3,
		label: 'Radio Management/Settings',
		direction: 'BOTH'
	},
	{
		value: 0xb4,
		label: 'Protocol Information',
		direction: 'BOTH'
	},
	{
		value: 0xb5,
		label: 'Network Information',
		direction: 'R2D'
	},
	{
		value: 0xb6,
		label: 'Volume Control',
		direction: 'D2R'
	},
	{
		value: 0xb7,
		label: 'Dialled String',
		direction: 'D2R'
	},
	{
		value: 0x01,
		label: 'Radio Test Request/Response',
		direction: 'BOTH'
	},
	{
		value: 0xc0,
		label: 'Positive Acknowledgement',
		direction: 'R2D'
	},
	{
		value: 0xd0,
		label: 'Queuing Acknowledgement',
		direction: 'R2D'
	},
	{
		value: 0xe0,
		label: 'Negative Acknowledgement',
		direction: 'R2D'
	}
];

/* @type {import('binary-decoder').MessageStructure} */
export const addressedMessageStructure = [
	{
		type: 'bits',
		parts: [
			{
				bits: 1
			},
			{
				bits: 7,
				id: 'PFIX1'
			},
			{
				bits: 3
			},
			{
				bits: 13,
				id: 'IDENT1'
			}
		]
	}
];

/* @type {import('binary-decoder').MessageStructure} */
export const addressStructure = [
	{
		type: 'bits',
		parts: [
			{
				bits: 4,
				id: 'ADESC',
				enums: [
					{
						value: 0,
						label: 'No addressing'
					},
					{
						value: 1,
						label: 'SAMIS BCD'
					},
					{
						value: 2,
						label: 'Spare'
					},
					{
						value: 3,
						label: 'MPT1327 ADDRESS'
					},
					{
						value: 4,
						label: 'MPT1327 PFIX/IDENT'
					},
					{
						value: 5,
						label: 'MPT1327 PFIX and IDENT'
					},
					{
						value: 6,
						label: 'MPT1327 PFIX, IDENT and SAMIS BCD'
					},
					{
						value: 7,
						label: 'MMI dialed number'
					}
				]
			},
			{
				bits: 4,
				id: 'LENGTH'
			}
		]
	},
	{
		type: 'variable',
		length: 'LENGTH',
		id: 'address'
	}
];

export const sstMessageStructure = [
	{
		type: 'bits',
		parts: [
			{
				bits: 4,
				id: 'CODING',
				enums: [
					{
						value: 3,
						label: 'NMEA'
					},
					{
						value: 4,
						label: 'IP Address'
					},
					{
						value: 5,
						label: '16-bit Unicode Characters'
					},
					{
						value: 6,
						label: 'MS Address'
					}
				]
			},
			{
				bits: 4,
				id: 'NLB'
			}
		]
	},
	{
		type: 'map',
		key: 'CODING',
		length: 'NLB',
		structures: {
			3: nmeaStructure,
			4: [
				{
					type: 'variable',
					id: 'ipAddress',
					length: 'NLB'
				}
			],
			5: [
				{
					type: 'string',
					id: 'message',
					length: 'NLB'
				}
			],
			6: [
				{
					type: 'string',
					id: 'msAddress',
					length: 'NLB'
				}
			]
		}
	}
];

/* @type {{[type: string]: import('binary-decoder').MessageStructure}} */
export const standardMessages = {
	0x81: [
		// Send/Receive SST message
		...addressedMessageStructure,
		...addressStructure,
		...sstMessageStructure
	],
	0x86: [
		// Normal disconnect/clear
		...addressedMessageStructure,
		{
			type: 'byte',
			id: 'cause',
			enums: [
				{
					value: 0x07,
					label: 'Normal Disconnect'
				}
			]
		}
	],
	0xa4: [
		// Setup/Incoming voice/modem
		...addressedMessageStructure,
		...addressStructure,
		{
			type: 'bits',
			parts: [
				{
					bits: 1,
					notes: 'Reserved'
				},
				{
					bits: 1,
					id: 'includeCall',
					format: Boolean
				},
				{
					bits: 1,
					id: 'groupCall',
					format: Boolean
				},
				{
					bits: 1,
					id: 'modemCall',
					format: Boolean
				},
				{
					bits: 1,
					id: 'highPriorityCall',
					format: Boolean
				},
				{
					bits: 3,
					id: 'callType',
					enums: [
						{
							value: 0,
							label: 'Standard call'
						}
					]
				}
			]
		}
	],
	0xa6: [
		// Cancelled Disconnect/Clear
		...addressedMessageStructure,
		{
			type: 'byte',
			id: 'cause',
			enums: [
				{
					value: 0x08,
					label: 'Cancel message/normal call set-up attempt'
				},
				{
					value: 0x09,
					label: 'Cancel include call set-up attempt'
				},
				{
					value: 0x0a,
					label: 'Abort diversion setting transaction'
				},
				{
					value: 0x0c,
					label: 'Cancel standard data call set-up attempt'
				}
			]
		}
	],
	0xb0: [
		// Radio Personality
		...addressedMessageStructure,
		{
			type: 'byte',
			id: 'manufacturerCode'
		},
		{
			type: 'bits',
			parts: [
				{
					bits: 4,
					id: 'model'
				},
				{
					bits: 2
				},
				{
					bits: 18,
					id: 'serialNumber'
				}
			]
		},
		{
			type: 'bits',
			id: 'capabilities',
			parts: [
				{
					bits: 1,
					id: 'voiceCalls',
					enums: [
						{
							value: 0,
							label: 'not supported'
						},
						{
							value: 1,
							label: 'supported'
						}
					]
				},
				{
					bits: 1,
					id: 'modemCalls',
					enums: [
						{
							value: 0,
							label: 'not supported'
						},
						{
							value: 1,
							label: 'supported'
						}
					]
				},
				{
					bits: 1,
					id: 'statusMessages',
					enums: [
						{
							value: 0,
							label: 'not supported'
						},
						{
							value: 1,
							label: 'supported'
						}
					]
				},
				{
					bits: 1,
					id: 'sstMessages',
					enums: [
						{
							value: 0,
							label: 'not supported'
						},
						{
							value: 1,
							label: 'supported'
						}
					]
				},
				{
					bits: 1,
					id: 'mstMessages',
					enums: [
						{
							value: 0,
							label: 'not supported'
						},
						{
							value: 1,
							label: 'supported'
						}
					]
				},
				{
					bits: 1,
					id: 'callDiversion',
					enums: [
						{
							value: 0,
							label: 'not supported'
						},
						{
							value: 1,
							label: 'supported'
						}
					]
				},
				{
					bits: 1,
					id: 'callbackLogging',
					enums: [
						{
							value: 0,
							label: 'not supported'
						},
						{
							value: 1,
							label: 'supported'
						}
					]
				},
				{
					bits: 1,
					id: 'stfFormat',
					enums: [
						{
							value: 0,
							label: 'not supported'
						},
						{
							value: 1,
							label: 'supported'
						}
					]
				}
			]
		},
		{
			type: 'bits',
			parts: [
				{
					// reserved
					bits: 8
				}
			]
		},
		{
			type: 'bits',
			parts: [
				{
					// for customisation
					bits: 8
				}
			]
		},
		{
			type: 'bits',
			id: 'supportedCodings',
			parts: [
				{
					bits: 1,
					id: 'bcdAsAscii',
					enums: [
						{
							value: 0,
							label: 'not supported'
						},
						{
							value: 1,
							label: 'supported'
						}
					]
				},
				{
					bits: 1,
					id: 'ccittAsAscii',
					enums: [
						{
							value: 0,
							label: 'not supported'
						},
						{
							value: 1,
							label: 'supported'
						}
					]
				},
				{
					bits: 1,
					id: 'binary',
					enums: [
						{
							value: 0,
							label: 'not supported'
						},
						{
							value: 1,
							label: 'supported'
						}
					]
				},
				{
					bits: 1,
					id: 'bcd',
					enums: [
						{
							value: 0,
							label: 'not supported'
						},
						{
							value: 1,
							label: 'supported'
						}
					]
				},
				{
					bits: 1,
					id: 'ccitt2',
					enums: [
						{
							value: 0,
							label: 'not supported'
						},
						{
							value: 1,
							label: 'supported'
						}
					]
				},
				{
					bits: 1,
					id: 'ccit5',
					enums: [
						{
							value: 0,
							label: 'not supported'
						},
						{
							value: 1,
							label: 'supported'
						}
					]
				},
				{
					bits: 1,
					id: 'pcCharset',
					enums: [
						{
							value: 0,
							label: 'not supported'
						},
						{
							value: 1,
							label: 'supported'
						}
					]
				},
				{
					// Reserved
					bits: 1
				}
			]
		}
	],
	0xb1: [
		// Numbering Information
		...addressedMessageStructure,
		{
			type: 'bits',
			parts: [
				{
					bits: 1,
					id: 'numberingScheme'
				},
				{
					bits: 15,
					id: 'individualBaseIdent'
				}
			]
		},
		{
			type: 'uint16',
			id: 'highestIndividualIdent'
		}
		// TODO
	],
	0xb2: [
		// Operating Condition (Radio control just has conditions)
		{
			type: 'bits',
			parts: [
				{
					bits: 5
				},
				{
					bits: 1,
					id: 'radioContact',
					enums: [
						{
							value: 0,
							label: 'Not in radio contact'
						},
						{
							value: 1,
							label: 'Is in radio contact'
						}
					]
				},
				{
					bits: 1,
					id: 'offHook',
					enums: [
						{
							value: 0,
							label: 'On-hook (idle or disconnected)'
						},
						{
							value: 1,
							label: 'Off-hook (active)'
						}
					]
				},
				{
					bits: 1,
					id: 'transmit',
					enums: [
						{
							value: 0,
							label: 'Not transmitting'
						},
						{
							value: 1,
							label: 'Transmitting'
						}
					]
				}
			]
		},
		{
			type: 'byte',
			id: 'fieldStrength'
		},
		{
			type: 'byte',
			id: 'mcdt',
			enums: [
				{
					value: 0xff,
					label: 'Call duration time infinite or not supported'
				}
			]
		}
	],
	0xb5: [
		// Network Information
		{
			type: 'bits',
			parts: [
				{
					bits: 6
				},
				{
					bits: 10,
					id: 'radioChannel'
				},
				{
					bits: 1
				},
				{
					bits: 15,
					id: 'systemIdentityCode'
				}
			]
		}
	],
	0xc4: [
		// Positive Setup Progress
		...addressedMessageStructure,
		...addressStructure,
		{
			type: 'byte',
			id: 'cause',
			enums: [
				{
					value: 0,
					label: 'Call connected'
				}
			]
		}
	],
	0xc5: [
		// Positive Receive Progress
		...addressedMessageStructure,
		...addressStructure,
		{
			type: 'byte',
			id: 'cause',
			enums: [
				{
					value: 0,
					label: 'Call connected'
				}
			]
		}
	],
	0xd0: [
		// Queuing Acknowledgement
		...addressedMessageStructure,
		...addressStructure,
		{
			type: 'byte',
			id: 'cause',
			enums: [
				{
					value: 0x02,
					label: 'System busy, wait for signalling'
				},
				{
					value: 0x0a,
					label: 'Called unit engaged, wait for signalling'
				}
			]
		}
	],
	0xe0: [
		// Negative Acknowledgement
		...addressedMessageStructure,
		...addressStructure,
		{
			type: 'byte',
			id: 'cause',
			enums: [
				{
					value: 0x08,
					label: 'Transaction aborted'
				},
				{
					value: 0x03,
					label: 'Invalid call, message rejected'
				},
				{
					value: 0x0b,
					label: 'System or called unit overload, message rejected'
				},
				{
					value: 0x04,
					label: 'Called radio out of reach or transaction abandoned'
				},
				{
					value: 0x0c,
					label: 'Called unit engagd or does not wish to accept message'
				},
				{
					value: 0x06,
					label: 'Called units calls are diverted'
				},
				{
					value: 0x16,
					label: 'Called units calls are diverted to a group address'
				},
				{
					value: 0x0e,
					label: 'Called units calls are diverted, but the diversion address is not available'
				},
				{
					value: 0xd,
					label: 'TSC does not support MST, aborted'
				}
			]
		}
	]
};

export const standardD2RMessages = {
	0xb0: [
		// Radio Interrogation (D2R only)
		{
			type: 'byte',
			id: 'reason',
			enums: [
				{
					value: 0x00,
					label: 'Personality Request'
				},
				{
					value: 0x01,
					label: 'Numbering Information'
				},
				{
					value: 0x02,
					label: 'Status of Radio Settings'
				},
				{
					value: 0x03,
					label: 'Operating Condition'
				},
				{
					value: 0x04,
					label: 'Network Information for Broadcasts'
				}
			]
		}
	]
};

export const standardStructure = [
	{
		type: 'uint8',
		id: 'messageType',
		enums: standardMessageTypes
	},
	{
		type: 'map',
		key: 'messageType',
		structures: standardMessages
	}
];

/**
 * Extend the MAP27 structure with additional message types
 *
 * @param {Array<import('binary-decoder').ValueEnum>} enums Enums for the
 *   message types
 * @param {Object<number, import('binary-decoder').MessageStructure>} structure
 *   Structure of additional messages
 * @param {import('binary-decoder').MessageStructure} [baseStructure] Base
 *   Structure to extend (defaults to the standard MAP27 structure
 */
export const extendStandard = (enums, structure, baseStructure) => {
	if (!baseStructure) {
		baseStructure = standardStructure;
	}

	return [
		{
			...baseStructure[0],
			enums: [...baseStructure[0].enums, ...enums]
		},
		{
			...standardStructure[1],
			structures: {
				...standardStructure[1].structures,
				...structure
			}
		}
	];
};

export default standardStructure;
