import { describe, it } from 'node:test';
import assert from 'node:assert/strict';
import taitStructure from '../messages/tait.js';
//import { createDecoder } from 'binary-decoder';
import { createAsciiDecoder } from '../decoder.js';

//const decoder = createDecoder(taitStructure);
const asciiDecoder = createAsciiDecoder(taitStructure);

/**
 *
 * @param {string} Message hex string
 */
//function hexToArray(message) {
//  return Uint8Array.from(message.match(/.{2}/g).map((byte) => parseInt(byte, 16)));
//}

/**
 * Convert ASCII hex string to byte array for string
 */
function asciiToArray(message) {
	return Uint8Array.from((message + '\r').split('').map((x) => x.charCodeAt()));
}

describe('NACK (0xe0)', () => {
	it('Decode out of reach or abandoned', () => {
		assert.deepEqual(asciiDecoder.decode(asciiToArray('e00405a80004')), {
			decodedMessage: {
				messageType: { value: 224, label: 'Negative Acknowledgement', direction: 'R2D' },
				gatewayDmrId: 263592,
				dmrGatewayIdDetect: 0,
				dmrId: 34216,
				PFIX1: 4,
				IDENT1: 1448,
				ADESC: { value: 0, label: 'No addressing' },
				LENGTH: 0,
				cause: {
					value: 4,
					label: 'Called radio out of reach or transaction abandoned'
				}
			},
			nextByte: 6
		});
	});
});

describe('Position call connected (0xc5)', () => {
	it('Decodes positive call connected', () => {
		assert.deepEqual(asciiDecoder.decode(asciiToArray('c50405a00000')), {
			decodedMessage: {
				messageType: {
					value: 197,
					label: 'Positive Receive Progress',
					direction: 'R2D'
				},
				gatewayDmrId: 263584,
				dmrGatewayIdDetect: 0,
				dmrId: 34208,
				PFIX1: 4,
				IDENT1: 1440,
				ADESC: { value: 0, label: 'No addressing' },
				LENGTH: 0,
				cause: { value: 0, label: 'Call connected' }
			},
			nextByte: 6
		});
	});
});

describe('setup/incoming voice/data (0xa4)', () => {
	it('decodes starting call', () => {
		assert.deepEqual(asciiDecoder.decode(asciiToArray('a40405a00000')), {
			decodedMessage: {
				messageType: {
					value: 164,
					label: 'Setup/Incoming voice/modem',
					direction: 'BOTH'
				},
				gatewayDmrId: 263584,
				dmrGatewayIdDetect: 0,
				dmrId: 34208,
				PFIX1: 4,
				IDENT1: 1440,
				ADESC: { value: 0, label: 'No addressing' },
				LENGTH: 0,
				includeCall: 0,
				groupCall: 0,
				modemCall: 0,
				highPriorityCall: 0,
				callType: { value: 0, label: 'Standard call' }
			},
			nextByte: 6
		});
	});

	it('decodes receiving a voice call', () => {
		assert.deepEqual(asciiDecoder.decode(asciiToArray('a40405a00000')), {
			decodedMessage: {
				messageType: {
					value: 164,
					label: 'Setup/Incoming voice/modem',
					direction: 'BOTH'
				},
				gatewayDmrId: 263584,
				dmrGatewayIdDetect: 0,
				dmrId: 34208,
				PFIX1: 4,
				IDENT1: 1440,
				ADESC: { value: 0, label: 'No addressing' },
				LENGTH: 0,
				includeCall: 0,
				groupCall: 0,
				modemCall: 0,
				highPriorityCall: 0,
				callType: { value: 0, label: 'Standard call' }
			},
			nextByte: 6
		});
	});
});

describe('DMR call status (0x0b)', () => {
	it('20 transmitting', () => {
		assert.deepEqual(asciiDecoder.decode(asciiToArray('0b0405a00001')), {
			decodedMessage: {
				messageType: { value: 11, label: 'DMR Call Status', direction: 'R2D' },
				gatewayDmrId: 263584,
				dmrGatewayIdDetect: 0,
				dmrId: 34208,
				PFIX1: 4,
				IDENT1: 1440,
				transmitting: 1
			},
			nextByte: 6
		});
	});
	it('20 stop transmitting', () => {
		assert.deepEqual(asciiDecoder.decode(asciiToArray('0b0000000000')), {
			decodedMessage: {
				messageType: { value: 11, label: 'DMR Call Status', direction: 'R2D' },
				gatewayDmrId: 0,
				dmrGatewayIdDetect: 0,
				dmrId: 0,
				PFIX1: 0,
				IDENT1: 0,
				transmitting: 0
			},
			nextByte: 6
		});
	});
});

describe('Network information (0xb5)', () => {
	it('decode network information', () => {
		assert.deepEqual(asciiDecoder.decode(asciiToArray('b54169c0ad')), {
			decodedMessage: {
				messageType: { value: 181, label: 'Network Information', direction: 'R2D' },
				radioChannel: 361,
				systemIdentityCode: 16557
			},
			nextByte: 5
		});
	});
});

describe('Radio/operating montrol (0xb2)', () => {
	it('Decode self transmitting', () => {
		assert.deepEqual(asciiDecoder.decode(asciiToArray('b2079cff')), {
			decodedMessage: {
				messageType: { value: 178, label: 'Radio/Operating Control', direction: 'BOTH' },
				radioContact: { value: 1, label: 'Is in radio contact' },
				offHook: { value: 1, label: 'Off-hook (active)' },
				transmit: { value: 1, label: 'Transmitting' },
				fieldStrength: 156,
				mcdt: {
					value: 255,
					label: 'Call duration time infinite or not supported'
				}
			},
			nextByte: 4
		});
	});

	it('Decode self stop transmitting', () => {
		assert.deepEqual(asciiDecoder.decode(asciiToArray('b2069bff')), {
			decodedMessage: {
				messageType: { value: 178, label: 'Radio/Operating Control', direction: 'BOTH' },
				radioContact: { value: 1, label: 'Is in radio contact' },
				offHook: { value: 1, label: 'Off-hook (active)' },
				transmit: { value: 0, label: 'Not transmitting' },
				fieldStrength: 155,
				mcdt: {
					value: 255,
					label: 'Call duration time infinite or not supported'
				}
			},
			nextByte: 4
		});
	});
});

describe('short message (0x81)', () => {
	it('Decodes receive NMEA message', () => {
		assert.deepEqual(
			asciiDecoder.decode(asciiToArray('810405a80037000001010000002907148faf041927091818')),
			{
				decodedMessage: {
					messageType: {
						value: 129,
						label: 'Send/Receive short data (SST) message',
						direction: 'BOTH'
					},
					gatewayDmrId: 263592,
					dmrGatewayIdDetect: 0,
					dmrId: 34216,
					PFIX1: 4,
					IDENT1: 1448,
					ADESC: { value: 0, label: 'No addressing' },
					LENGTH: 0,
					CODING: { value: 3, label: 'NMEA' },
					NLB: 7,
					encrypted: { value: 0, label: 'Not encrypted' },
					northSouth: { value: 0, label: 'South' },
					EastWest: { value: 1, label: 'East' },
					Quality: { value: 1, label: 'Valid fix' },
					speed: 0,
					heading: 0,
					latDegrees: 41,
					latMinutes: 7,
					latFraction: 5263,
					longDegrees: 175,
					longMinutes: 4,
					longFraction: 6439,
					utcHours: 9,
					utcMinutes: 24,
					utcSeconds: 24
				},
				nextByte: 24
			}
		);
	});
});

describe('Ident (0xb0)', () => {
	it('Decodes radio personality', () => {
		assert.deepEqual(asciiDecoder.decode(asciiToArray('b00405b304d04fdafe0000ef')), {
			decodedMessage: {
				messageType: {
					value: 176,
					label: 'Radio Interrogation/Personality',
					direction: 'BOTH'
				},
				gatewayDmrId: 263603,
				dmrGatewayIdDetect: 0,
				dmrId: 34227,
				PFIX1: 4,
				IDENT1: 1459,
				manufacturerCode: 4,
				model: 13,
				serialNumber: 20442,
				capabilities: {
					voiceCalls: {
						value: 1,
						label: 'supported'
					},
					modemCalls: {
						value: 1,
						label: 'supported'
					},
					statusMessages: {
						value: 1,
						label: 'supported'
					},
					sstMessages: {
						value: 1,
						label: 'supported'
					},
					mstMessages: {
						value: 1,
						label: 'supported'
					},
					callDiversion: {
						value: 1,
						label: 'supported'
					},
					callbackLogging: {
						value: 1,
						label: 'supported'
					},
					stfFormat: {
						value: 0,
						label: 'not supported'
					}
				},
				supportedCodings: {
					bcdAsAscii: {
						value: 1,
						label: 'supported'
					},
					ccittAsAscii: {
						value: 1,
						label: 'supported'
					},
					binary: {
						value: 1,
						label: 'supported'
					},
					bcd: {
						value: 0,
						label: 'not supported'
					},
					ccitt2: {
						value: 1,
						label: 'supported'
					},
					ccit5: {
						value: 1,
						label: 'supported'
					},
					pcCharset: {
						value: 1,
						label: 'supported'
					}
				}
			},
			nextByte: 12
		});
	});
});

describe('Queueing ACK (0xd0)', () => {
	it('asciiDecode decodes system busy', () => {
		assert.deepEqual(asciiDecoder.decode(asciiToArray('d00405a80002')), {
			decodedMessage: {
				messageType: { value: 208, label: 'Queuing Acknowledgement', direction: 'R2D' },
				PFIX1: 4,
				IDENT1: 1448,
				ADESC: { value: 0, label: 'No addressing' },
				LENGTH: 0,
				cause: { value: 2, label: 'System busy, wait for signalling' }
			},
			nextByte: 6
		});
	});
});
