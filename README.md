# map27 0.2.0

MAP27 message decoder/encoder

[![pipeline status](https://gitlab.com/bytesnz/map27/badges/main/pipeline.svg)](git+https://gitlab.com/bytesnz/map27/commits/main)
[![map27 on NPM](https://bytes.nz/b/map27/npm)](https://npmjs.com/package/map27)
[![license](https://bytes.nz/b/map27/custom?color=yellow&name=license&value=AGPL-3.0)](git+https://gitlab.com/bytesnz/map27/blob/main/LICENSE)
[![developtment time](https://bytes.nz/b/map27/custom?color=yellowgreen&name=development+time&value=~5+hours)](git+https://gitlab.com/bytesnz/map27/blob/main/.tickings)
[![contributor covenant](https://bytes.nz/b/map27/custom?color=purple&name=contributor+covenant&value=v1.4.1+adopted)](git+https://gitlab.com/bytesnz/map27/blob/main/CODE_OF_CONDUCT.md)
[![support development](https://bytes.nz/b/map27/custom?color=brightgreen&name=support+development&value=$$)](https://liberapay.com/MeldCE)

**!Currently only contains an decoder!**

A decoder and encoder for the MPT1327 mobile access protocol (MAP) MAP27. It
includes coders for standard MAP27 messages, as well as additional
[Tait Communications][tait] radio messages.

## Example

```js
import { createMap27Coder, createTaitCoder, createTaitAsciiCoder } from 'map27';

let coder = createTaitCoder();
let message = new Uint8Array([0x84, 0x04, 0x05, 0xa8, 0x21, 0xa5, 0x05]);

console.log(coder.decode(message));

/*
Output will be:
  {
    decodedMessage: {
      messageType: { value: 132, label: 'DMR Poll' },
      gatewayDmrId: 263592,
      dmrGatewayIdDetect: 0,
      dmrId: 34216,
      PFIX1: 4,
      IDENT1: 1448,
      ADESC: { value: 2, label: 'Spare' },
      LENGTH: 1,
      address: Uint8Array(1) [ 165 ],
      pollFormat: { value: 5, label: 'NMEA' }
    },
    nextByte: 7
  }
*/
```

## Development

Feel free to post errors or feature requests to the project
[issue tracker](https://gitlab.com/bytesnz/map27/issues) or
[email](mailto:contact-project+bytesnz-map27-37096438-issue-@incoming.gitlab.com) them to us.
**Please submit security concerns as a
[confidential issue](https://gitlab.com/bytesnz/map27/issues?issue[confidential]=true)**

The source is hosted on [Gitlab](git+https://gitlab.com/bytesnz/map27) and uses
[eslint][], [prettier][], [lint-staged][] and [husky][] to keep things pretty.
As such, when you first [clone][git-clone] the repository, as well as
installing the npm dependencies, you will also need to install [husky][].

```bash
# Install NPM dependencies
npm install
# Set up husky Git hooks stored in .husky
npx husky install
```

# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [0.2.0] - 2024-11-23

### Added

- Basic testing
- Numbering information message

## [0.1.2] - 2023-03-04

### Added

- Decoding of 0xd0 (ACK queue) messages

### Changed

- Updated dependencies

## [0.1.1] - 2022-08-08

### Fixed

- Fixed enums for NMEA message format
- Changed 0xb0 radio interrogation (D2R) for radio personality (R2D) message
  as prefer dealing with R2D messages at the moment

## [0.1.0] - 2022-06-15

Initial version with common message decoder only

[0.2.0]: https://gitlab.com/bytesnz/map27/-/compare/v0.1.2...v0.2.0
[0.1.2]: https://gitlab.com/bytesnz/map27/-/compare/v0.1.1...v0.1.2
[0.1.1]: https://gitlab.com/bytesnz/map27/-/compare/v0.1.0...v0.1.1
[0.1.0]: https://gitlab.com/bytesnz/map27/-/tree/v0.1.0


[husky]: https://typicode.github.io/husky
[eslint]: https://eslint.org/
[git-clone]: https://www.git-scm.com/docs/git-clone
[prettier]: https://prettier.io/
[lint-staged]: https://github.com/okonet/lint-staged#readme
[gitlab-ci]: https://docs.gitlab.com/ee/ci/
