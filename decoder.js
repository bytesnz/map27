import { createDecoder } from 'binary-decoder';

/**
 * Create an asciiDecoder
 */
export const createAsciiDecoder = (messages, options) => {
	const decoder = createDecoder(messages, options);

	return {
		/**
		 * Decode an MAP27 message
		 *
		 * @param {Uint8Array} message Message to decode
		 */
		decode: (message) => {
			let length = message.length;
			if (message.length % 2 == 1) {
				if (message[message.length - 1] != '\r'.charCodeAt(0)) {
					throw new Error('Message should be even or end with \\r');
				} else {
					length--;
				}
			}

			const decodedMessage = [];
			let i = 0;
			while (i < length) {
				let hex = String.fromCharCode(message[i++]) + String.fromCharCode(message[i++]);
				let value = parseInt(hex, 16);
				decodedMessage.push(value);
			}

			return decoder.decode(Uint8Array.from(decodedMessage));
		}
	};
};
