import taitStructure from './messages/tait.js';
import standardStructure from './messages/standard.js';
import { createDecoder } from 'binary-decoder';
import { createAsciiDecoder } from './decoder.js';

const decoderOptions = {
	littleEndian: false,
	throwOnInvalidEnumValue: false
};

/** @typedef {object} Options
 *
 * @property {import('binary-decoder').Logger} logger Logger object to pass
 *   to the coder
 */

/**
 * Create a standard MAP27 coder
 *
 * @param {Options} [options] Options to pass to the coder
 */
export const createMap27Coder = (options) =>
	createDecoder(standardStructure, {
		...(options || {}),
		...decoderOptions
	});

/**
 * Create a Tait Radio MAP27 coder
 *
 * @param {Options} [options] Options to pass to the coder
 */
export const createTaitCoder = (options) =>
	createDecoder(taitStructure, {
		...(options || {}),
		...decoderOptions
	});

/**
 * Create a Tait Radio ASCII (Hex) Encoded MAP27 coder
 *
 * @param {Options} [options] Options to pass to the coder
 */
export const createTaitAsciiCoder = (options) =>
	createAsciiDecoder(taitStructure, {
		...(options || {}),
		...decoderOptions
	});
