# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [0.2.0] - 2024-11-23

### Added

- Basic testing
- Numbering information message

## [0.1.2] - 2023-03-04

### Added

- Decoding of 0xd0 (ACK queue) messages

### Changed

- Updated dependencies

## [0.1.1] - 2022-08-08

### Fixed

- Fixed enums for NMEA message format
- Changed 0xb0 radio interrogation (D2R) for radio personality (R2D) message
  as prefer dealing with R2D messages at the moment

## [0.1.0] - 2022-06-15

Initial version with common message decoder only

[0.2.0]: https://gitlab.com/bytesnz/map27/-/compare/v0.1.2...v0.2.0
[0.1.2]: https://gitlab.com/bytesnz/map27/-/compare/v0.1.1...v0.1.2
[0.1.1]: https://gitlab.com/bytesnz/map27/-/compare/v0.1.0...v0.1.1
[0.1.0]: https://gitlab.com/bytesnz/map27/-/tree/v0.1.0
