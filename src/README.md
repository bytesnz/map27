# <!=package.json name> <!=package.json version>

<!=package.json description>

[![pipeline status](https://gitlab.com/bytesnz/<!=package.json name>/badges/main/pipeline.svg)](<!=package.json repository.url>/commits/main)
[![<!=package.json name> on NPM](https://bytes.nz/b/<!=package.json name>/npm)](https://npmjs.com/package/<!=package.json name>)
[![license](https://bytes.nz/b/<!=package.json name>/custom?color=yellow&name=license&value=AGPL-3.0)](<!=package.json repository.url>/blob/main/LICENSE)
[![developtment time](https://bytes.nz/b/<!=package.json name>/custom?color=yellowgreen&name=development+time&value=~5+hours)](<!=package.json repository.url>/blob/main/.tickings)
[![contributor covenant](https://bytes.nz/b/<!=package.json name>/custom?color=purple&name=contributor+covenant&value=v1.4.1+adopted)](<!=package.json repository.url>/blob/main/CODE_OF_CONDUCT.md)
[![support development](https://bytes.nz/b/<!=package.json name>/custom?color=brightgreen&name=support+development&value=$$)](https://liberapay.com/MeldCE)

**!Currently only contains an decoder!**

A decoder and encoder for the MPT1327 mobile access protocol (MAP) MAP27. It
includes coders for standard MAP27 messages, as well as additional
[Tait Communications][tait] radio messages.

## Example

```js
import { createMap27Coder, createTaitCoder, createTaitAsciiCoder } from 'map27';

let coder = createTaitCoder();
let message = new Uint8Array([0x84, 0x04, 0x05, 0xa8, 0x21, 0xa5, 0x05]);

console.log(coder.decode(message));

/*
Output will be:
  {
    decodedMessage: {
      messageType: { value: 132, label: 'DMR Poll' },
      gatewayDmrId: 263592,
      dmrGatewayIdDetect: 0,
      dmrId: 34216,
      PFIX1: 4,
      IDENT1: 1448,
      ADESC: { value: 2, label: 'Spare' },
      LENGTH: 1,
      address: Uint8Array(1) [ 165 ],
      pollFormat: { value: 5, label: 'NMEA' }
    },
    nextByte: 7
  }
*/
```

## Development

Feel free to post errors or feature requests to the project
[issue tracker](<!=package.json bugs.url>) or
[email](mailto:<!=package.json bugs.email>) them to us.
**Please submit security concerns as a
[confidential issue](<!=package.json bugs.url>?issue[confidential]=true)**

The source is hosted on [Gitlab](<!=package.json repository.url>) and uses
[eslint][], [prettier][], [lint-staged][] and [husky][] to keep things pretty.
As such, when you first [clone][git-clone] the repository, as well as
installing the npm dependencies, you will also need to install [husky][].

```bash
# Install NPM dependencies
npm install
# Set up husky Git hooks stored in .husky
npx husky install
```

<!=CHANGELOG.md>

[husky]: https://typicode.github.io/husky
[eslint]: https://eslint.org/
[git-clone]: https://www.git-scm.com/docs/git-clone
[prettier]: https://prettier.io/
[lint-staged]: https://github.com/okonet/lint-staged#readme
[gitlab-ci]: https://docs.gitlab.com/ee/ci/
